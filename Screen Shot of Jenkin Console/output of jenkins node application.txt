Started by user Pravin Nikum
Running as SYSTEM
Building in workspace C:\Users\Admin\.jenkins\workspace\Node Application
The recommended git tool is: NONE
No credentials specified
Cloning the remote Git repository
Cloning repository https://gitlab.com/D7_56814_Pravin/d7_56814_pravin_sdm.git
 > git.exe init C:\Users\Admin\.jenkins\workspace\Node Application # timeout=10
Fetching upstream changes from https://gitlab.com/D7_56814_Pravin/d7_56814_pravin_sdm.git
 > git.exe --version # timeout=10
 > git --version # 'git version 2.33.0.windows.2'
 > git.exe fetch --tags --force --progress -- https://gitlab.com/D7_56814_Pravin/d7_56814_pravin_sdm.git +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git.exe config remote.origin.url https://gitlab.com/D7_56814_Pravin/d7_56814_pravin_sdm.git # timeout=10
 > git.exe config --add remote.origin.fetch +refs/heads/*:refs/remotes/origin/* # timeout=10
Avoid second fetch
 > git.exe rev-parse "refs/remotes/origin/master^{commit}" # timeout=10
Checking out Revision 20fe956145be5e26cbe6eb4f13fb800a4302f657 (refs/remotes/origin/master)
 > git.exe config core.sparsecheckout # timeout=10
 > git.exe checkout -f 20fe956145be5e26cbe6eb4f13fb800a4302f657 # timeout=10
Commit message: "screen shot of git repository and postman windows updated"
First time build. Skipping changelog.
[Node Application] $ cmd /c call C:\Users\Admin\AppData\Local\Temp\jenkins4520086630054344256.bat

C:\Users\Admin\.jenkins\workspace\Node Application>docker-compose down  
nodeapplication  Warning: No resource found to remove

C:\Users\Admin\.jenkins\workspace\Node Application>docker-compose build 
#2 [nodeapplication_server internal] load build definition from Dockerfile
#2 sha256:2427ad188242a5b1dfe4df14350f4f43c6cdbfed12ed966c02c97e1acc80ad1c
#2 transferring dockerfile: 294B 0.0s done
#2 ...

#1 [nodeapplication_db internal] load build definition from Dockerfile
#1 sha256:18c6b83ea3c5c8ec10d5f0ef8b9541c58b1b81c859ac7e34d06dcdd89ea813a8
#1 transferring dockerfile: 282B 0.1s done
#1 DONE 0.3s

#2 [nodeapplication_server internal] load build definition from Dockerfile
#2 sha256:2427ad188242a5b1dfe4df14350f4f43c6cdbfed12ed966c02c97e1acc80ad1c
#2 DONE 0.3s

#3 [nodeapplication_db internal] load .dockerignore
#3 sha256:40981eb31b0f7804dc50f8253cde820b538b4930ec6cc30eb9e2f35ea7ccbb17
#3 transferring context: 0.1s
#3 transferring context: 2B 0.1s done
#3 DONE 0.3s

#4 [nodeapplication_server internal] load .dockerignore
#4 sha256:509e5d8240c96b88ff65834d0310e1c31047ca0755e79bf26f449955ccfe8a7f
#4 transferring context: 2B 0.1s done
#4 DONE 0.1s

#5 [nodeapplication_db internal] load metadata for docker.io/library/mysql:latest
#5 sha256:f4914bf881bba81f49673260093dead2b629e18e4d67c76939b1f813bb051069
#5 DONE 0.0s

#9 [nodeapplication_server internal] load metadata for docker.io/library/node:latest
#9 sha256:1c0b05b884068c98f7acad32e4f7fd374eba1122b4adcbb1de68aa72d5a6046f
#9 DONE 0.0s

#10 [nodeapplication_server 1/3] FROM docker.io/library/node
#10 sha256:5045d46e15358f34ea7fff145af304a1fa3a317561e9c609f4ae17c0bd3359df
#10 DONE 0.0s

#6 [nodeapplication_db 1/2] FROM docker.io/library/mysql
#6 sha256:a5d39839c39cd4bd7fb960856a647e2bb12c7b06ec949f6bf97790e3436e389c
#6 DONE 0.0s

#12 [nodeapplication_server internal] load build context
#12 sha256:b4ad972b80d90a46a814d876454258312c6f4f8a26b25fca8806377f8981fe87
#12 ...

#7 [nodeapplication_db internal] load build context
#7 sha256:e9a129695b55947993d6098082398a8ec9525e14cae4a9a620d795855e126f82
#7 transferring context: 193B 0.6s done
#7 DONE 0.7s

#6 [nodeapplication_db 1/2] FROM docker.io/library/mysql
#6 sha256:a5d39839c39cd4bd7fb960856a647e2bb12c7b06ec949f6bf97790e3436e389c
#6 CACHED

#12 [nodeapplication_server internal] load build context
#12 sha256:b4ad972b80d90a46a814d876454258312c6f4f8a26b25fca8806377f8981fe87
#12 transferring context: 3.46MB 0.7s done
#12 DONE 0.8s

#11 [nodeapplication_server 2/3] WORKDIR /app
#11 sha256:9c069f6fc249db0a1d261845aed20bc20c1a5fdae3d2cbc7f8f5ef59eb080214
#11 CACHED

#13 [nodeapplication_server 3/3] COPY . .
#13 sha256:1d2c82129036cb3a4cd402a55e2c579305fe4af1cfb7143011cdd8c7f27d4801
#13 ...

#8 [nodeapplication_db 2/2] COPY db.sql /docker-entrypoint-initdb.d/
#8 sha256:49fa6eb8d1a9975159cba4ea88635c56005b88c3f5010a2338332895260e6a10
#8 DONE 0.2s

#14 [nodeapplication_db] exporting to image
#14 sha256:e8c613e07b0b7ff33893b694f7759a10d42e180f2b4dc349fb57dc6b71dcab00
#14 exporting layers 0.1s done
#14 writing image sha256:4f9df583f808ab4b89e8bb9115ffe087a1babe8d6aba5da30c0a4bb409af2d1e 0.0s done
#14 naming to docker.io/library/nodeapplication_db
#14 naming to docker.io/library/nodeapplication_db done
#14 DONE 0.2s

#13 [nodeapplication_server 3/3] COPY . .
#13 sha256:1d2c82129036cb3a4cd402a55e2c579305fe4af1cfb7143011cdd8c7f27d4801
#13 DONE 0.6s

#14 [nodeapplication_server] exporting to image
#14 sha256:e8c613e07b0b7ff33893b694f7759a10d42e180f2b4dc349fb57dc6b71dcab00
#14 exporting layers 0.2s done
#14 writing image sha256:8183909598c432ff6974397a2d307ff9fb111c4e92dcfbe9f803a11dc33804d3 done
#14 naming to docker.io/library/nodeapplication_server done
#14 DONE 0.2s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them

C:\Users\Admin\.jenkins\workspace\Node Application>docker-compose up -d 
Network nodeapplication_default  Creating
Network nodeapplication_default  Created
Container nodeapplication_server_1  Creating
Container nodeapplication_db_1  Creating
Container nodeapplication_server_1  Created
Container nodeapplication_db_1  Created
Container nodeapplication_server_1  Starting
Container nodeapplication_db_1  Starting
Container nodeapplication_server_1  Started
Container nodeapplication_db_1  Started

C:\Users\Admin\.jenkins\workspace\Node Application>exit 0 
Finished: SUCCESS
