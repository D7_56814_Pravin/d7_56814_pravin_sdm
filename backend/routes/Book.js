const express=require(`express`)
const db=require('../db')
const utils=require('../utils')
const cryptoJs=require('crypto-js')
const e=require('express')
const { response, request, Router } = require('express')
const { CLIENT_SECURE_CONNECTION } = require('mysql/lib/protocol/constants/client')



const router=express.Router()

 router.post('/',(request,response)=>{
     const{book_title,publisher_name,author_name}=request.body
     const connection=db.openConnection()
     const statement=`insert into book(book_title,publisher_name,author_name) values('${book_title}','${publisher_name}','${author_name}')`
     connection.query(statement,(error,result)=>{
         connection.end()
         response.send(utils.createResult(error,result))
     })
 })

 router.get('/',(request,response)=>{
     const connection=db.openConnection()
     const statement=`select * from book`
     connection.query(statement,(error,result)=>{
         if(error)
         {
             response.send(utils.createResult(error))
         }
         else if(result.length==0)
         {
             response.send(utils.createResult('no user found'))
         }
         else 
         {
             connection.end()
             response.send(utils.createResult(null,result))
         }
     })

 })

 router.put('/:id',(request,response)=>{
     const{publisher_name,author_name}=request.body
     const{id}=request.params
     const connection=db.openConnection()
     const statement=`update book set publisher_name='${publisher_name}',author_name='${author_name}'where id='${id}'`
     connection.query(statement,(error,result)=>{
         connection.end()
         response.send(utils.createResult(error,result))
     })

 })

 router.delete('/:id',(request,response)=>{
     const{id}=request.params
     const connection=db.openConnection()
     const statement=`delete from book where id='${id}'`
     connection.query(statement,(error,result)=>{
         connection.end()
         response.send(utils.createResult(error,result))
     })
 })
 router.delete('/:id',(request,response)=>{
    const{id}=request.params
    const connection=db.openConnection()
    const statement=`delete from book where id='${id}'`
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})

module.exports=router
